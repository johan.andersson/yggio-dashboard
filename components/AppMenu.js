import { connect } from 'react-redux'
import Link from 'next/link'
import Button from '@material-tailwind/react/Button'
import Icon from '@material-tailwind/react/Icon'
import { toggleAppMenu } from '../redux/actions/actions'

function AppMenu({ open }) {
  return (
    <div className={`fixed flex shadow-md flex-wrap bg-white w-64 h-auto top-24 m-2 right-0 p-2 ${open ? 'transition duration-300' : 'transition -translate-y-full duration-300 hidden'} `}>
     
     <Link href='/'>
        <a className='flex justify-center items-center'>
          <Button
            color='gray'
            buttonType='outline'
            rounded={false}
            iconOnly={true}
            ripple='dark'
            className='md:inline-flex h-20 w-20 border-0'
            onClick={toggleAppMenu}
          >
            <Icon name='home' size='2xl'/>
            Dash
          </Button>
        </a>
      </Link>
      
      <Link href='/devices'>
        <a className='flex justify-center items-center'>
          <Button
            color='gray'
            buttonType='outline'
            rounded={false}
            iconOnly={true}
            ripple='dark'
            className='md:inline-flex h-20 w-20 border-0'
            onClick={toggleAppMenu}
          >
            <Icon name='devices' size='2xl'/>
            Devices
          </Button>
        </a>
      </Link>

      <Link href='/install'>
        <a className='flex justify-center items-center'>
          <Button
            color='gray'
            buttonType='outline'
            rounded={false}
            iconOnly={true}
            ripple='dark'
            className='md:inline-flex h-20 w-20 border-0'
            onClick={toggleAppMenu}
          >
            <Icon name='storage' size='2xl'/>
            Install
          </Button>
        </a>
      </Link>

      <Button
        color='gray'
        buttonType='outline'
        rounded={false}
        iconOnly={true}
        ripple='dark'
        className='md:inline-flex h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='bar_chart' size='2xl'/>
        Charts
      </Button>

      <Button
        color='gray'
        buttonType='outline'
        rounded={false}
        iconOnly={true}
        ripple='dark'
        className='md:inline-flex h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='corporate_fare' size='2xl'/>
        Org.
      </Button>

      <Button
        color='gray'
        buttonType='outline'
        rounded={false}
        iconOnly={true}
        ripple='dark'
        className='md:inline-flex h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='rule' size='2xl'/>
        Rules
      </Button>

      <Button
        color='gray'
        buttonType='outline'
        rounded={false}
        iconOnly={true}
        ripple='dark'
        className='md:inline-flex h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='place' size='2xl'/>
        Locations
      </Button>

      <Button
        color='gray'
        buttonType='outline'
        rounded={false}
        iconOnly={true}
        ripple='dark'
        className='md:inline-flex h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='settings' size='2xl'/>
        Settings
      </Button>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    open: state.appMenu.open
  }
}

export default connect(
  mapStateToProps
)(AppMenu)
