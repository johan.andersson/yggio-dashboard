import Button from '@material-tailwind/react/Button'
import Icon from '@material-tailwind/react/Icon'
import Image from 'next/image'
import YggioPic from '../public/index.png'
import { connect } from 'react-redux'
import AppMenu from './AppMenu'
import Notifications from './Notifications'
import { toggleSidebar, toggleAppMenu, toggleNotifications } from '../redux/actions/actions'
import Link from 'next/link'

function Header() {
  return (
    <header className='fixed w-full top-0 z-50 flex items-center justify-between px-4 py-2 shadow-md bg-gradient-to-t from-green-900 to-green-800'>
      
      <Button
        color='green-900'
        buttonType='outline'
        rounded={true}
        iconOnly={true}
        ripple='dark'
        className='inline-flex h-20 w-20 border-0'
        onClick={toggleSidebar}
      >
        <Icon name='menu' size='3xl' color='white'/>
      </Button>
      
      <Link href='/'>
        <a className='flex justify-center items-center ml-2'>
          <Image src={YggioPic} height={30} width={30} layout='fixed' className='color-red'/>
          <h1 className='px-2 mr-5 text-white'>
            Yggio
          </h1>
        </a>
      </Link>

      <div className=' mx-5 md:!mx-20 flex md:!flex-grow items-center px-5 py-2 
      bg-gray-100 text-gray-600 rounded-lg focus-within:text-gray-600 focus-within:shadow-md'>
        <Icon name='search' size='3xl' color='gray'/>
        <input type='text' placeholder='Search' 
        className='hidden md:!flex flex-grow px-5 text-base bg-transparent outline-none'/>
      </div>

      <Button
        color='green-900'
        buttonType='outline'
        rounded={true}
        iconOnly={true}
        ripple='dark'
        className='sm:hidden md:inline-flex ml-5 md:ml-5 h-20 w-20 border-0'
        onClick={toggleAppMenu}
      >
        <Icon name='apps' size='2xl' color='white'/>
        
      </Button>

      

      <Button
        color='green-900'
        buttonType='outline'
        rounded={true}
        iconOnly={true}
        ripple='dark'
        className='sm:hidden md:inline-flex h-20 w-20 border-0'
        onClick={toggleNotifications}
      >
        <Icon name='notifications' size='2xl' color='white'/>
        <span className='animate-ping absolute ml-5 mb-5 inline-flex h-3 w-3 rounded-full bg-green-200 opacity-75'></span>
        <span className='absolute ml-5 mb-5 inline-flex rounded-full h-2 w-2 bg-gray-200'></span>
      </Button>

      <Button
        color='green-900'
        buttonType='outline'
        rounded={true}
        iconOnly={true}
        ripple='dark'
        className='sm:hidden md:inline-flex h-20 w-20 border-0'
      >
        <Icon name='account_circle' size='2xl' color='white'/>
      </Button>
      <AppMenu />
      <Notifications />
    </header>
  )
}

const mapDispatchToProps = { toggleAppMenu }

export default connect(
  null,
  mapDispatchToProps
)(Header)
