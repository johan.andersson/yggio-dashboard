import { connect } from 'react-redux'

function Notifications({ open }) {
  return (
    <div className={`fixed shadow-md flex flex-wrap bg-white w-64 h-auto top-24 m-2 right-0 p-2 ${open ? 'transition duration-300' : 'transition -translate-y-full duration-300 hidden'} `}>
     notiser
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    open: state.notifications.open
  }
}

export default connect(
  mapStateToProps
)(Notifications)
