import { connect } from 'react-redux'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import * as All from './sidebars/index'

function SidebarMenu({ open, children }) {

//  useEffect(() => {
//    const sidebar = document.querySelector('#sidebar')
//    sidebar.addEventListener('transitionend', () =>{
//      console.log('jadå')
//      
//      sidebar.classList.add('hidden')
//    })
//  })

  const router = useRouter()
  const TagName = getSidebarMenu(router.pathname)

  return (
    <div id='sidebar' className={`z-50 fixed md:!relative overflow-hidden bg-white shadow-md h-full ${open ? 'w-72 transition-width duration-300' : 'w-0 transition-width w-0 duration-300'} `}>
      <div className='w-72 py-8 px-4'>
      {children}
      </div> 
    </div>
  )
}

const getSidebarMenu = pathname => {
  return `All.${pathname}Bar`.replace('/', '')
}

const mapStateToProps = (state) => {
  return {
    open: state.sidebar.open
  }
}

export default connect(
  mapStateToProps
)(SidebarMenu)
