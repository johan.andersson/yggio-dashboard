import 'tailwindcss/tailwind.css'
import '../styles/base.css'
import Head from 'next/head'
import Header from '../components/Header'
import AppMenu from '../components/AppMenu'
import SidebarMenu from '../components/SidebarMenu'
import { Provider } from 'react-redux'
import { useStore } from '../redux/store'
import store from '../redux/store'


function Yggio({ Component, pageProps }) {
  //const store = useStore(pageProps.initialReduxState)

  return (
    <Provider store={store}>
      <div className='h-full flex bg-gray-200'>
        <Head>
          <link
            href='https://fonts.googleapis.com/icon?family=Material+Icons'
            rel='stylesheet'
          />
        </Head>

        <Header />
        
        <Component {...pageProps} />
        
        
      </div>
  </Provider>
  )
}

export default Yggio
