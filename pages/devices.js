import Head from 'next/head'
import '@material-tailwind/react/tailwind.css'
import { connect } from 'react-redux'
import SidebarMenu from '../components/SidebarMenu'
import Icon from '@material-tailwind/react/Icon'
import Button from '@material-tailwind/react/Button'
import Input from '@material-tailwind/react/Input'
import Header from '../components/Header'
import { toggleDevicesNameFilter } from '../redux/actions/actions'

function Devices({open, nameOpen}) {
  return (
    <div className={`pt-24 flex flex-grow w-full h-full`}>
      <Head>
        <title>Yggio | Devices</title>
        <link rel="icon" href="/index.png" />
      </Head>

      <SidebarMenu>
        <div className='flex w-full justify-between'>
          <div className='flex items-center'>
            <Icon name='filter_list' size='2xl'/> <h3 className='mx-2'>Filter</h3>
          </div>
          <Button
            color='red'
            buttonType='outline'
            size='sm'
            rounded={false}
            block={false}
            iconOnly={false}
            ripple='dark'
            className='px-2'
          >
            Clear filter
          </Button>
        </div>
        <ul className='mt-4'>
          <li className='flex flex-col'>
            <div className='flex w-full items-center justify-between cursor-pointer' onClick={toggleDevicesNameFilter}>
              Name <Icon name='expand_more' size='2xl'/>
            </div>
            <div className={`box-border my-2 mt-4 overflow-hidden ${nameOpen ? 'h-10 transition-height duration-300' : 'h-0 transition-height duration-300'}`}>
              <Input
                type='text'
                color='green'
                size='sm'
                outline={true}
                placeholder='Name...'
                className=''
              />
            </div>
            
          </li>
          <li className='flex items-center justify-between py-2'>
            Device Model Name <Icon name='expand_more' size='2xl'/>
          </li>
          <li className='flex items-center justify-between py-2'>
            Type <Icon name='expand_more' size='2xl'/>
          </li>
        </ul>
      </SidebarMenu>

      <div className={`border-box flex-grow h-full ${open ? 'transition duration-300' : 'transition duration-300'}`}>
        <div className='m-4'>
          <div className='flex items-center justify-between'>
            <div className='flex items-center'>
              <h4>Devices</h4>
              <div className='border rounded-xl p-0.5 pl-2 pr-2 text-xs bg-gray-300 ml-2'>
                32
              </div>
            </div>
            <div className='flex'>
              <Button
                  color='gray'
                  buttonType='outline'
                  size='sm'
                  rounded={false}
                  iconOnly={false}
                  ripple='dark'
                  className='mr-2 z-40'
                >
                Custumize columns
              </Button>

              <Button
                  color='gray'
                  buttonType='outline'
                  size='sm'
                  rounded={false}
                  iconOnly={false}
                  ripple='dark'
                  className='mr-2'
                >
                Select many
              </Button>

              <Button
                color='green'
                buttonType='filled'
                size='sm'
                rounded={false}
                iconOnly={false}
                ripple='dark'
                
              >
              New device
            </Button>
            </div>
          </div>
          <div className='bg-white p-4 mt-2'>
          
          </div>
        </div>
      </div>
      
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    open: state.sidebar.open,
    nameOpen: state.devicesFilters.nameOpen
  }
}

export default connect(
  mapStateToProps
)(Devices)
