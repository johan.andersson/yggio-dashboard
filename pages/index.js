import Head from 'next/head'
import '@material-tailwind/react/tailwind.css'
import { connect } from 'react-redux'
import {Line} from 'react-chartjs-2'
import SidebarMenu from '../components/SidebarMenu'

import Header from '../components/Header'

const data = {
  labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
  datasets: [{
    label: 'Temperature',
    data: [12, 19, 3, 5, 2, 3],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1
  },
  {
    label: 'Temperature2',
    data: [10, 1, 3, 5, 2, 3],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
    ],
    borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
    ],
    borderWidth: 1
  }
  ]
}

function Home({open}) {
  return (
    <div className={`pt-24 flex flex-grow ${open ? 'transition duration-300' : 'transition -translate-x-72 duration-300'} `}>
      <Head>
        <title>Yggio | Dashboard</title>
        <link rel="icon" href="/index.png" />
      </Head>
      <SidebarMenu />
      <div className='w-full h-full'>
        HEJ HE JEH 
        <div className='w-72 h-72'>
        <Line
          data={data}
          width={300}
          height={150}
          options={{
            maintainAspectRatio: false
          }}
        />
        </div>
      </div>
      
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    open: state.sidebar.open
  }
}

export default connect(
  mapStateToProps
)(Home)
