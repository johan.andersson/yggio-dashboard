import Head from 'next/head'
import '@material-tailwind/react/tailwind.css'
import { connect } from 'react-redux'
import SidebarMenu from '../components/SidebarMenu'

import Header from '../components/Header'

function Install({open}) {
  return (
    <div className={`pt-24 flex flex-grow ${open ? 'transition duration-300' : 'transition -translate-x-72 duration-300'} `}>
      <Head>
        <title>Yggio | Dashboard</title>
        <link rel="icon" href="/index.png" />
      </Head>

      <SidebarMenu />
      <div>
        INSTALL
      </div>
      
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    open: state.sidebar.open
  }
}

export default connect(
  mapStateToProps
)(Install)
