import { TOGGLE_SIDEBAR } from './actionTypes'
import { TOGGLE_APP_MENU } from './actionTypes'
import { TOGGLE_NOTIFICATIONS } from './actionTypes'
import { TOGGLE_DEVICES_NAME_FILTER } from './actionTypes'

import store from '../store'

export const toggleSidebar = () => (
  console.log('HEJK'),
  
  store.dispatch({
    type: TOGGLE_SIDEBAR
  })
)

export const toggleAppMenu = () => (
  console.log('MENYYY'),

  store.dispatch({
    type: TOGGLE_APP_MENU
  })
)


export const toggleNotifications = () => (
  console.log('NOTIS'),

  store.dispatch({
    type: TOGGLE_NOTIFICATIONS
  })
)

export const toggleDevicesNameFilter = () => (
  console.log('dev name'),

  store.dispatch({
    type: TOGGLE_DEVICES_NAME_FILTER
  })
)