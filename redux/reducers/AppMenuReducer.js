import { TOGGLE_APP_MENU } from '../actions/actionTypes'

const initialState = {
  open: false
}

export default function(state = initialState, action) {
  console.log('toggle')
  switch (action.type) {
    case TOGGLE_APP_MENU: {
      console.log('toggle igen')
      return {
        ...state,
        open: !state.open
      }
    }
    default:
      return state
  }
}
