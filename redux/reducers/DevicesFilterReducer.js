import { TOGGLE_DEVICES_NAME_FILTER } from '../actions/actionTypes'

const initialState = {
  nameOpen: false
}

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_DEVICES_NAME_FILTER: {
      console.log('toggle')
      return {
        ...state,
        nameOpen: !state.nameOpen
      }
    }
    default:
      return state
  }
}
