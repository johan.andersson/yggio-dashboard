import { TOGGLE_NOTIFICATIONS } from '../actions/actionTypes'

const initialState = {
  open: false
}

export default function(state = initialState, action) {
  console.log('toggle')
  switch (action.type) {
    case TOGGLE_NOTIFICATIONS: {
      console.log('toggle not')
      return {
        ...state,
        open: !state.open
      }
    }
    default:
      return state
  }
}
