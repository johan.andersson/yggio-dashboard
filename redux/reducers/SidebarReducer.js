import { TOGGLE_SIDEBAR } from '../actions/actionTypes'

const initialState = {
  open: true
}

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_SIDEBAR: {
      console.log('toggle')
      return {
        ...state,
        open: !state.open
      }
    }
    default:
      return state
  }
}
