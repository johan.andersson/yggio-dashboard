import { combineReducers } from 'redux'
import SidebarReducer from './SidebarReducer'
import AppMenuReducer from './AppMenuReducer'
import NotificationsReducer from './NotificationsReducer'
import DevicesFilterReducer from './DevicesFilterReducer'

export default combineReducers({ sidebar: SidebarReducer, appMenu: AppMenuReducer, notifications: NotificationsReducer, devicesFilters: DevicesFilterReducer})
