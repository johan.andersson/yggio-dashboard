import { useMemo } from 'react'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import reducer from "./reducers"

//let store

//const initialState = {
//  lastUpdate: 0,
//  light: false,
//  count: 0,
//}

const saveToLocalStorage = state => {
  if (process.browser) {
    try {
      const serialisedState = JSON.stringify(state)
      localStorage.setItem('persistantState', serialisedState)
    } catch (e) {
      console.warn(e)
    }
  }
}

const loadFromLocalStorage = () => {
  if (process.browser) {
    try {
      const serialisedState = localStorage.getItem('persistantState')
      if (serialisedState === null) return undefined
      return JSON.parse(serialisedState)
    } catch (e) {
      console.warn(e)
      return undefined
    }
  }
}

const middleware = [thunk]

const store = createStore(reducer, loadFromLocalStorage(), composeWithDevTools(applyMiddleware(...middleware)))

store.subscribe(() => saveToLocalStorage(store.getState()))

export default store

//function initStore(preloadedState = initialState) {
//  return createStore(
//    reducer,
//    preloadedState,
//    composeWithDevTools(applyMiddleware(...middleware))
//  )
//}
//
//
//export const initializeStore = (preloadedState) => {
//  let _store = store ?? initStore(preloadedState)
//
//  // After navigating to a page with an initial Redux state, merge that state
//  // with the current state in the store, and create a new store
//  if (preloadedState && store) {
//    _store = initStore({
//      ...store.getState(),
//      ...preloadedState,
//    })
//    // Reset the current store
//    store = undefined
//  }
//
//  // For SSG and SSR always create a new store
//  if (typeof window === 'undefined') return _store
//  // Create the store once in the client
//  if (!store) store = _store
//
//  return _store
//}
//
//export function useStore(initialState) {
//  const store = useMemo(() => initializeStore(initialState), [initialState])
//  store.subscribe(() => saveToLocalStorage(store.getState()))
//  return store
//}
